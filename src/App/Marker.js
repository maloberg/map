import './App'
const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js')
export class Marker
{
    title;
    description;
    strtDate;
    endDate;
    lng;
    lat;
    marker;
    constructor(json)
    {
        this.title = json.title;
        this.description = json.description;
        this.strtDate = json.startingDate;
        this.endDate = json.endingDate;
 
        this.timestampStrtDate = new Date(json.startingDate).getTime();
        this.timestampEndDate = new Date(json.endingDate)
        this.lng = json.lng;
        this.lat = json.lat;
        this.markerCreator();
    }
    markerCreator()
    {
        const color = this.colorChoice();

        this.marker = new mapboxgl.Marker({color: color});
        const strtDate = this.strtDate.split('T'); 
        strtDate[0] = strtDate[0].split('-');

        const endDate = this.endDate.split('T');
        endDate[0] = endDate[0].split('-');
        this.marker.getElement().title = `${this.title}  
        Debut: ${strtDate[0][2]}-${strtDate[0][1]}-${strtDate[0][0]}
        Fin: ${endDate[0][2]}-${endDate[0][1]}-${endDate[0][0]}`;

        const day = 60*60*24*1000;
        const hour = day/24;
        const min = hour/60;
        let remainingDays = Math.floor((Date.parse(this.strtDate) - Date.now())/day)
        let remainingHours = Math.floor(((Date.parse(this.strtDate)- Date.now())%day)/hour)
        let remainingMinutes = Math.floor((((Date.parse(this.strtDate) - Date.now())%day)%hour)/min)
        
        if(color === 'green')
        this.marker.setPopup(new mapboxgl.Popup().setHTML(`<p>Debut: ${strtDate[0][2]}-${strtDate[0][1]}-${strtDate[0][0]} à ${strtDate[1]}<br>Fin: ${endDate[0][2]}-${endDate[0][1]}-${endDate[0][0]} à ${endDate[1]}</p><h1>${this.title}</h1><p>${this.description}</p>`));

        else if(color === 'red')
        this.marker.setPopup(new mapboxgl.Popup().setHTML(`<p>Debut: ${strtDate[0][2]}-${strtDate[0][1]}-${strtDate[0][0]} à ${strtDate[1]}<br>Fin: ${endDate[0][2]}-${endDate[0][1]}-${endDate[0][0]} à ${endDate[1]}</p><h1>${this.title}</h1><p>${this.description}</p><p>vous avez rate l'evenement</p>`));

        else if(color === 'orange')
        this.marker.setPopup(new mapboxgl.Popup().setHTML(`<p>Debut: ${strtDate[0][2]}-${strtDate[0][1]}-${strtDate[0][0]} à ${strtDate[1]}<br>Fin: ${endDate[0][2]}-${endDate[0][1]}-${endDate[0][0]} à ${endDate[1]}</p><h1>${this.title}</h1><p>${this.description}</p><p>l'evenement est en cours</p>`));

        else if(color === 'yellow')
        this.marker.setPopup(new mapboxgl.Popup().setHTML(`<p>Debut: ${strtDate[0][2]}-${strtDate[0][1]}-${strtDate[0][0]} à ${strtDate[1]}<br>Fin: ${endDate[0][2]}-${endDate[0][1]}-${endDate[0][0]} à ${endDate[1]}</p><h1>${this.title}</h1><p>${this.description}</p><p>l'evement est dans ${remainingDays} jours ${remainingHours} heures et ${remainingMinutes} minutes</p>`));
        
        this.marker.setLngLat([this.lng, this.lat]); 
    };

    colorChoice()
    {
        let color;
        const actualTime = Date.now();
        const timestampStrtDate = new Date(this.strtDate).getTime();
        const timestampEndDate = new Date(this.endDate).getTime();
        const days = 3
        const delayBeforeEvt = 60 * 60 * 24 * days * 1000;

        if(timestampEndDate - actualTime < 0)
            color = 'red';

        else if(timestampStrtDate - actualTime < 0)
            color = 'orange';

        else if(actualTime + delayBeforeEvt > timestampStrtDate)
            color = 'yellow';

        else
            color = 'green';

        return color
    }

    toJSON() 
    {
        return {
            title: this.title,
            description: this.description,
            startingDate: this.strtDate,
            endingDate: this.endDate,
            lng: this.lng,
            lat: this.lat
        }
    }
}