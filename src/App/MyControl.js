import app from'./App'
import './Marker'

export class MyControl {
	onAdd(map) 
	{
		this._map = map;
		this._container = document.createElement('div');
		const button = document.createElement('button');
		button.innerText = 'reinitialiser les dates';
		button.setAttribute('type','button');
		this._container.className = 'button-reset';

		button.addEventListener('click',this.onClick);
		this._container.appendChild(button);
		return this._container;
	}
	 
	onRemove() 
	{
		this._container.parentNode.removeChild(this._container);
		this._map = undefined;
	}

	onClick()
    {
		console.log('bob');
		for(let marker of app.markers)
		{
			marker.marker.remove();    
		}
		app.markers = [];
		app.loadItems();
	}			
}