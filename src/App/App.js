import '../index.html';
import 'mapbox-gl/dist/mapbox-gl.css';
import '../style.css';
import key from'../../app.config'
import { Marker } from './Marker';
import {MyControl} from './MyControl'
const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js')

class App {

    domEvtTitle;
    domEvtDescription;
    domDateStrtEvt;
    domDateEndEvt;
    domLng;
    domLat;
    domBtnNewAdd;
    domBtnResetDate;
    map;
    markers = [];

    start() 
    {
        this.initHTML();
        this.mapCreator();
        this.loadItems();
    };

    loadItems()
    {
        const localStorageContent = localStorage.getItem(key.localStorageName);
        if(localStorageContent === null)
        {
            return;
        };

        let storedJson = JSON.parse(localStorageContent);
        for(let json of storedJson)
        {
            const marker = new Marker(json);
            this.markers.push(marker);
        }
        this.renderItems();
    }

    renderItems()
    {        
        
        for(let marker of this.markers)
        {
            marker.marker.addTo( this.map ); 
        };
    }
    
    initHTML()
    {
        this.domEvtTitle = document.querySelector('#event-title'); 
        this.domEvtTitle.addEventListener('focus',this.onErrorRemove.bind(this));

        this.domEvtDescription = document.querySelector('#event-description');
        this.domEvtDescription.addEventListener('focus',this.onErrorRemove.bind(this));

        this.domDateStrtEvt = document.querySelector('#starting-date');
        this.domDateStrtEvt.addEventListener('focus',this.onErrorRemove.bind(this));

        this.domDateEndEvt = document.querySelector('#ending-date');
        this.domDateEndEvt.addEventListener('focus',this.onErrorRemove.bind(this));

        this.domLng = document.querySelector('#lng');
        this.domLng.value = 2.7561596;
        this.domLng.addEventListener('focus',this.onErrorRemove.bind(this));

        this.domLat = document.querySelector('#lat');
        this.domLat.value = 42.6341848;
        this.domLat.addEventListener('focus',this.onErrorRemove.bind(this));

        this.domBtnNewAdd = document.querySelector('#new-add');
        this.domBtnNewAdd.addEventListener('click',this.onButtonNewAdd.bind(this));
    };

    mapCreator()
    {
        mapboxgl.accessToken = key.keyName;

        this.map = new mapboxgl.Map(
        {
            container: 'map',
            minZoom: 5,
            center:{lng:2.7561596   , lat:42.6341848},
            zoom:10,
            style: 'mapbox://styles/mapbox/streets-v11'
        });

        this.fonctionalities();        
        this.configMap();
    };


    fonctionalities()
    {
        this.map.on('click', e =>
        {
            this.domLng.value = e.lngLat.lng;
            this.domLat.value = e.lngLat.lat;
        });

    };
    
    configMap()
    {

        this.map.addControl(new mapboxgl.NavigationControl(), 'top-left');


        this.map.addControl(new MyControl, 'top-left')
    };    

    onErrorRemove(evt)
    {
        evt.target.classList.remove('error');
    }

    onButtonNewAdd()
    {
        if(this.inputsTest())
        return;

        const newJson = 
        {
            title: this.domEvtTitle.value,
            description: this.domEvtDescription.value,
            startingDate: this.domDateStrtEvt.value,
            endingDate: this.domDateEndEvt.value,
            lng: this.domLng.value,
            lat: this.domLat.value
        }

        this.domEvtTitle.value = this.domEvtDescription.value = this.domDateStrtEvt.value = this.domDateEndEvt.value = '';
        const newMarker = new Marker(newJson);
        
        newMarker.marker.addTo(this.map);
        console.log(newMarker.marker);
        this.markers.push(newMarker);
        localStorage.setItem( key.localStorageName, JSON.stringify( this.markers ));
    };

    inputsTest()
    {
        let hasError = false;
        const newTitle = this.domEvtTitle.value.trim();
        if(newTitle === '')
        {
            this.domEvtTitle.classList.add('error');
            this.domEvtTitle.value = '';

            hasError = true;
        };

        const newDescritption = this.domEvtDescription.value.trim(); 
        if(newDescritption === '')
        {
            this.domEvtDescription.classList.add('error');
            this.domEvtDescription.value = '';

            hasError = true;
        };

        const newStartingDate = this.domDateStrtEvt.value;
        if(newStartingDate === '')
        {
            this.domDateStrtEvt.classList.add('error');
            this.domDateStrtEvt.value = '';

            hasError = true;
        };

        const newEndingDate = this.domDateEndEvt.value;
        if(newEndingDate === '' || new Date(newEndingDate).getTime() < new Date(newStartingDate).getTime())
        {
            this.domDateEndEvt.classList.add('error');
            this.domDateEndEvt.value = '';
            hasError = true;
        };

        const newLng = this.domLng.value.trim();
        if(newLng === '')
        {
            this.domLng.classList.add('error');
            this.domLng.value = '';
            hasError = true;
        };

        const newLat = this.domLat.value.trim();
        if(newLat === '')
        {
            this.domLat.classList.add('error');
            this.domLat.value = '';
            hasError = true;
        };
    
        return hasError;
    }
}


const instance = new App();

export default instance;